import { FC } from "react";
import Navbar from "../../component/navbar/index";
import CardLibrary from "../../component/Library/CardLibrary/index.js";
import AddButton from "../../component/Library/AddButton/index.js";
import "./index.css";
import React from "react";
import Box from "@mui/material/Box";
import { Grid } from "@mui/material";
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';

const Library: FC = () => {
  // const dataBooks = localStorage.getItem("books");

  // console.log(dataBooks);

  return (
    <>
      <Navbar />
      <Card className="card" sx={{borderRadius:8, backgroundColor:"#8DA9C4"}}>
      <Typography className="h1" 
      marginTop={5}
      marginLeft={8}  
      variant="h3" 
      gutterBottom 
      component="div" 
      fontFamily={'nuito'}>
      Book Library
        </Typography>
      <Grid
        container
        direction="row"
        alignItems="center"
        spacing={1}
        my="3px"
        mx={1}
        className="gridbook"
      >
        <CardLibrary />
        <AddButton />
      </Grid>
      </Card>
    </>
  );
};

export default Library;
