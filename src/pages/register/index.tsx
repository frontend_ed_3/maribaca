import { FC } from "react";
import React from "react";
import {
  TextField,
  Checkbox,
  Link,
  Paper,
  Box,
  Grid,
  Typography,
  FormControlLabel,
  Button,
  Container,
} from "@mui/material";
import "./index.css";

const Register: FC = () => {
  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <Typography
            my="35px"
            mx={5}
            color="#EEF4ED"
            fontWeight="bold"
            fontSize="32px"
            variant="h4"
          >
            MARIBACA
          </Typography>
        </Grid>
        <Grid item xs={5}>
          <Box
            className="register-box"
            sx={{
              my: 16,
              mx: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "left",
              backgroundColor: "#EEF4ED",
              borderRadius: "20px",
              width: "550px",
              height: "450px",
              position: "relative",
            }}
          >
            <Container component="form" sx={{ mt: 1 }}>
              <Typography
                mt="25px"
                color="#134074"
                variant="h4"
                fontWeight="bold"
              >
                Create new account.
              </Typography>
              <Typography color="#134074" variant="h5">
                it's free!
              </Typography>
              <TextField
                sx={{ width: "100%", marginTop: "1rem" }}
                margin="normal"
                id="username"
                label="Username"
                name="username"
                autoComplete="username"
                variant="outlined"
                size="small"
                autoFocus
              />
              <br />
              <TextField
                sx={{ width: "100%", marginTop: "1rem" }}
                type="email"
                margin="normal"
                label="Email"
                name="email"
                autoComplete="email"
                variant="outlined"
                size="small"
                autoFocus
              />
              <br />
              <TextField
                sx={{ width: "100%" }}
                type="password"
                margin="normal"
                label="Password"
                name="password"
                autoComplete="current-password"
                variant="outlined"
                size="small"
              />
              <br />
              <button className="registerSubmitButton">create account</button>
              <Typography color="#134074" mt="30px" textAlign="center">
                Already have an account?
                <Link href="/" ml="5px" fontWeight="bold">
                  Sign in
                </Link>
              </Typography>
            </Container>
          </Box>
        </Grid>
        <Grid item xs={1}></Grid>
      </Grid>
    </>
  );
};

export default Register;
