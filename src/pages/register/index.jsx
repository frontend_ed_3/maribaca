import React from "react";
import {
  TextField,
  Link,
  Box,
  Grid,
  Typography,
  Container,
  Button,
} from "@mui/material";
import "./index.css";
import { useFormik } from "formik";
import * as Yup from "yup";

function InputErrorMessage({ touched, errors, inputName }) {
  return (
    <Typography variant="caption" color="red">
      {touched[inputName] && errors[inputName]}
    </Typography>
  );
}

const Register = () => {
  const {
    touched,
    errors,
    getFieldProps,
    isValid,
    dirty,
    isSubmitting,
    handleSubmit,
  } = useFormik({
    initialValues: {
      username: "",
      email: "",
      password: "",
    },
    validationSchema: Yup.object().shape({
      username: Yup.string().required("Username is required"),
      email: Yup.string()
        .email("Please enter a valid email")
        .required("Email is required"),
      password: Yup.string()
        .required("Password is required")
        .min(8, "Password must be at least 8 characters long"),
    }),
    async onSubmit(values) {
      alert(JSON.stringify(values));
      console.log(values);
    },
  });

  const defaultErrorMessageProps = {
    touched,
    errors,
  };

  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <Typography
            my="35px"
            mx={5}
            color="#EEF4ED"
            fontWeight="bold"
            fontSize="32px"
            variant="h4"
            fontFamily={"nuito"}
          >
            MARIBACA
          </Typography>
        </Grid>
        <Grid item xs={5}>
          <Box
            sx={{
              my: 16,
              mx: 4,
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              backgroundColor: "#EEF4ED",
              borderRadius: "20px",
              width: "550px",
              height: "450px",
              position: "relative",
            }}
          >
            <Container sx={{ mt: 1 }}>
              <Typography
                mt="10px"
                color="#134074"
                variant="h4"
                fontWeight="bold"
                fontFamily={"nuito"}
              >
                Create new account.
              </Typography>
              <Typography color="#134074" variant="h5" fontFamily={"nuito"}>
                it's free!
              </Typography>
              <form onSubmit={handleSubmit}>
                <TextField
                  sx={{ width: "100%", marginTop: "1%" }}
                  id="username"
                  name="username"
                  margin="normal"
                  label="Username"
                  autoComplete="username"
                  variant="outlined"
                  size="small"
                  autoFocus
                  {...getFieldProps("username")}
                />
                <InputErrorMessage
                  {...defaultErrorMessageProps}
                  inputName="username"
                />
                <br />
                <TextField
                  sx={{ width: "100%", marginTop: "1%" }}
                  id="email"
                  name="email"
                  type="email"
                  margin="normal"
                  label="Email"
                  autoComplete="email"
                  variant="outlined"
                  size="small"
                  {...getFieldProps("email")}
                />
                <InputErrorMessage
                  {...defaultErrorMessageProps}
                  inputName="email"
                />
                <br />
                <TextField
                  sx={{ width: "100%", marginTop: "1%" }}
                  id="password"
                  name="password"
                  type="password"
                  margin="normal"
                  label="Password"
                  autoComplete="current-password"
                  variant="outlined"
                  size="small"
                  {...getFieldProps("password")}
                />
                <InputErrorMessage
                  {...defaultErrorMessageProps}
                  inputName="password"
                />
                <br />
                <Box sx={{ textAlign: "center" }}>
                  <Button
                    type="submit"
                    disabled={!(isValid && dirty) || isSubmitting}
                    variant="contained"
                    href="/dashboard"
                    sx={{
                      backgroundColor: "#134074",
                      color: "white",
                      "&:hover": {
                        backgroundColor: "#13315C",
                        color: "white",
                      },
                      fontWeight: "bold",
                      textTransform: "none",
                    }}
                  >
                    create account
                  </Button>
                </Box>
              </form>
              <Typography
                color="#134074"
                mt="30px"
                textAlign="center"
                fontFamily={"nuito"}
              >
                Already have an account?
                <Link href="http://localhost:3000" ml="5px" fontWeight="bold">
                  Sign in
                </Link>
              </Typography>
            </Container>
          </Box>
        </Grid>
        <Grid item xs={1}></Grid>
      </Grid>
    </>
  );
};

export default Register;
