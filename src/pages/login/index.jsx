import React from "react";
import {
  TextField,
  Link,
  Box,
  Grid,
  Typography,
  Container,
  Button,
} from "@mui/material";
import "./index.css";
import { useFormik } from "formik";
import * as Yup from "yup";

function InputErrorMessage({ touched, errors, inputName }) {
  return (
    <Typography variant="caption" color="red" fontFamily={'nuito'}>
      {touched[inputName] && errors[inputName]}
    </Typography>
  );
}

const Login = () => {
  const {
    touched,
    errors,
    getFieldProps,
    isValid,
    dirty,
    isSubmitting,
    handleSubmit,
  } = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: Yup.object().shape({
      email: Yup.string()
        .email("Please enter a valid email")
        .required("Email is required"),
      password: Yup.string()
        .required("Password is required")
        .min(8, "Password must be at least 8 characters long"),
    }),
    async onSubmit(values) {
      alert(JSON.stringify(values));
      console.log(values);
    },
  });

  const defaultErrorMessageProps = {
    touched,
    errors,
  };

  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <Typography
            my="35px"
            mx={5}
            color="#EEF4ED"
            fontWeight="bold"
            fontSize="32px"
            variant="h4"
            fontFamily={'nuito'}
          >
            MARIBACA
          </Typography>
        </Grid>
        <Grid item xs={5}>
          <Box
            sx={{
              my: 16,
              mx: 4,
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              backgroundColor: "#EEF4ED",
              borderRadius: "20px",
              maxWidth: "550px",
              height: "450px",
              position: "relative",
            }}
          >
            <Container sx={{ mt: 1 }}>
              <Typography
                mt="5px"
                color="#134074"
                variant="h4"
                fontWeight="bold"
                fontFamily={'nuito'}
              >
                To start using
              </Typography>
              <Typography color="#134074" variant="body1" fontFamily={'nuito'}>
                Enter your details to get sign in to your account
              </Typography>
              <form onSubmit={handleSubmit}>
                <TextField
                  sx={{ width: "100%", marginTop: "1%" }}
                  id="email"
                  name="email"
                  type="email"
                  margin="normal"
                  label="Enter your email address"
                  autoComplete="email"
                  variant="outlined"
                  size="small"
                  autoFocus
                  {...getFieldProps("email")}
                />
                <InputErrorMessage
                  {...defaultErrorMessageProps}
                  inputName="email"
                />
                <br />
                <TextField
                  sx={{ width: "100%", marginTop: "1%" }}
                  id="password"
                  name="password"
                  type="password"
                  margin="normal"
                  label="Enter your password"
                  autoComplete="current-password"
                  variant="outlined"
                  size="small"
                  {...getFieldProps("password")}
                />
                <InputErrorMessage
                  {...defaultErrorMessageProps}
                  inputName="password"
                />
                <br />
                <Typography fontFamily={'nuito'}>
                  <Link href="#">can't sign in?</Link>
                </Typography>
                <br />
                <Box sx={{ textAlign: "center" }}>
                  <Button
                    type="submit"
                    disabled={!(isValid && dirty) || isSubmitting}
                    variant="contained"
                    href="/dashboard"
                    sx={{
                      backgroundColor: "#134074",
                      color: "white",
                      "&:hover": {
                        backgroundColor: "#13315C",
                        color: "white",
                      },
                      fontWeight: "bold",
                      textTransform: "none",
                    }}
                  >
                    sign in
                  </Button>
                </Box>
              </form>
              <Typography color="#134074" mt="30px" textAlign="center"fontFamily={'nuito'}>
                Don't have an account yet?
                <Link href="/register" ml="5px" fontWeight="bold">
                  Create Now!
                </Link>
              </Typography>
            </Container>
          </Box>
        </Grid>
        <Grid item xs={1}></Grid>
      </Grid>
    </>
  );
};

export default Login;
