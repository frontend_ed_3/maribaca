import { FC } from "react";
import React from "react";
import {
  TextField,
  Box,
  Grid,
  Typography,
  Container,
} from "@mui/material";
import "./index.css";

const PasswordReset: FC = () => {
  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <Typography
            my="35px"
            mx={5}
            color="#EEF4ED"
            fontWeight="bold"
            fontSize="32px"
            variant="h4"
            fontFamily={'nuito'}
          >
            MARIBACA
          </Typography>
        </Grid>
        <Grid item xs={5}>
          <Box
            sx={{
              my: 16,
              mx: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "left",
              backgroundColor: "#EEF4ED",
              borderRadius: "20px",
              width: "550px",
              height: "450px",
            }}
          >
            <Container component="form" sx={{ mt: 1 }}>
              <Typography variant="h2">🔑</Typography>
              <Typography color="#134074" variant="h4" fontWeight="bold">
                Set your new password
              </Typography>
              <Typography color="#134074" variant="body1">
                your new password must be different from your previous password.
              </Typography>
              <TextField
                sx={{ width: "50%" }}
                margin="normal"
                type="password"
                label="Enter your new password"
                name="password"
                autoComplete="new-password"
                variant="outlined"
                size="small"
              />
              <br />
              <TextField
                sx={{ width: "50%" }}
                margin="normal"
                type="password"
                label="Confirm your new password"
                name="password"
                autoComplete="confirm-password"
                variant="outlined"
                size="small"
              />
              <br />
              <button className="resetSubmitButton">reset password</button>
            </Container>
          </Box>
        </Grid>
        <Grid item xs={1}></Grid>
      </Grid>
    </>
  );
};

export default PasswordReset;
