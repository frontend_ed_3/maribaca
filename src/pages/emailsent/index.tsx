import { FC } from "react";
import React from "react";
import {
  Link,
  Box,
  Grid,
  Typography,
  Container,
} from "@mui/material";
import ArrowBackRoundedIcon from "@mui/icons-material/ArrowBackRounded";
import "./index.css";

const EmailSent: FC = () => {
  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <Typography
            my="35px"
            mx={5}
            color="#EEF4ED"
            fontWeight="bold"
            fontSize="32px"
            variant="h4"
            fontFamily={'nuito'}
          >
            MARIBACA
          </Typography>
        </Grid>
        <Grid item xs={5}>
          <Box
            sx={{
              my: 16,
              mx: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "left",
              backgroundColor: "#EEF4ED",
              borderRadius: "20px",
              width: "550px",
              height: "450px",
            }}
          >
            <Container component="form" sx={{ mt: 1 }}>
              <Typography mt="10px" fontFamily={'nuito'}>
                <Link href="#" underline="none">
                  <ArrowBackRoundedIcon />
                  Back to sign in
                </Link>
              </Typography>
              <Typography variant="h2" textAlign="center" mt="15%" fontFamily={'nuito'}>📩</Typography>
              <Typography textAlign="center" color="#134074" variant="h5" fontWeight="bold" fontFamily={'nuito'}>
                Check your email
              </Typography>
              <Typography textAlign="center" color="#134074" variant="subtitle1" fontFamily={'nuito'}>
                we’ve sent you a link to reset your password
              </Typography>
              <button className="openEmailSubmitButton">open email</button>
            </Container>
          </Box>
        </Grid>
        <Grid item xs={1}></Grid>
      </Grid>
    </>
  );
};

export default EmailSent;
