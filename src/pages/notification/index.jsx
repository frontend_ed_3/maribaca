import React from "react";
import "./index.css";
import PrimarySearchAppBar from "../../component/navbar/index";
import { Grid } from "@mui/material";
import NotificationCard from "../../component/notificationCard";

const NotificationsPage = () => {
  

  return (
    <>
      <div className="page">
        <PrimarySearchAppBar/>        
        <Grid container 
        direction="row"
        justifyContent="center"
        alignItems="center"
        sx={{ height:700}}
        >
          <Grid item container 
          justifyContent="center"
          alignItems="center" xs={12}
          sx={{ height:700}}>
            <NotificationCard/>
          </Grid>   
                 
        </Grid>      
      </div>
    </>
  );
};

export default NotificationsPage;
