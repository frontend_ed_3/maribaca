import { FC } from "react";
import React from "react";
import {
  TextField,
  Link,
  Box,
  Grid,
  Typography,
  Container,
} from "@mui/material";
import ArrowBackRoundedIcon from "@mui/icons-material/ArrowBackRounded";
import "./index.css";

const ForgotPassword: FC = () => {
  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <Typography
            my="35px"
            mx={5}
            color="#EEF4ED"
            fontWeight="bold"
            fontSize="32px"
            variant="h4"
            fontFamily={'nuito'}
          >
            MARIBACA
          </Typography>
        </Grid>
        <Grid item xs={5}>
          <Box
            sx={{
              my: 16,
              mx: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "left",
              backgroundColor: "#EEF4ED",
              borderRadius: "20px",
              width: "550px",
              height: "450px",
            }}
          >
            <Container component="form" sx={{ mt: 1 }}>
              <Typography mt="10px" fontFamily={'nuito'}>
                <Link href="/" underline="none">
                  <ArrowBackRoundedIcon />
                  Back to sign in
                </Link>
              </Typography>
              <Typography variant="h2" textAlign="center" mt="20px" fontFamily={'nuito'}>🤔</Typography>
              <Typography textAlign="center" color="#134074" variant="h4" fontWeight="bold" fontFamily={'nuito'}>
                Forgot your password?
              </Typography>
              <Typography textAlign="center" color="#134074" variant="subtitle1" fontFamily={'nuito'}>
                no worries, we’re help you get your account back
              </Typography>
              <TextField
                sx={{ marginBottom: "1rem" }}
                type="email"
                margin="normal"
                label="Enter your email address"
                name="email"
                autoComplete="email"
                variant="outlined"
                size="small"
                fullWidth
                autoFocus
              />
              <br />
              <button className="forgotPasswordSubmitButton">
                reset password
              </button>
            </Container>
          </Box>
        </Grid>
        <Grid item xs={1}></Grid>
      </Grid>
    </>
  );
};

export default ForgotPassword;
