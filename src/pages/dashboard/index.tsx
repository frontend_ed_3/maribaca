import { FC } from "react";
import React from "react";
import "./index.css";
import PrimarySearchAppBar from "../../component/navbar/index";
import LibraryMiniCard from "../../component/libraryMiniCard/";
import LevelMiniCard from "../../component/levelMiniCard/index.jsx";
import { Grid } from "@mui/material";
import ProgressCard from "../../component/progressCard";

const Dashboard: FC = () => {
  return (
    <>
      <div className="page">
        <PrimarySearchAppBar/>        
        <Grid container 
        direction="row"
        justifyContent="center"
        alignItems="center"
        sx={{ height:700}}
        >
          <Grid item          
          container 
          direction="column"
          justifyContent="center"
          alignItems="center"
          sx={{ height:700}}          
          xs={5}
          >          
            <LevelMiniCard/>
            <LibraryMiniCard/>
          </Grid>
          <Grid item container 
          justifyContent="center"
          alignItems="center" xs={7}
          sx={{ height:700}}>
            <ProgressCard/>
          </Grid>   
                 
        </Grid>      
      </div>
    </>
  );
};

export default Dashboard;
