import { FC } from "react";
import React from "react";
import {
  Box,
  Grid,
  Typography,
  Container,
} from "@mui/material";
import "./index.css";

const ResetSuccess: FC = () => {
  return (
    <>
      <Grid container spacing={3}>
        <Grid item xs={6}>
          <Typography
            my="35px"
            mx={5}
            color="#EEF4ED"
            fontWeight="bold"
            fontSize="32px"
            variant="h4"
            fontFamily={'nuito'}
          >
            MARIBACA
          </Typography>
        </Grid>
        <Grid item xs={5}>
          <Box
            sx={{
              my: 16,
              mx: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "left",
              backgroundColor: "#EEF4ED",
              borderRadius: "20px",
              width: "550px",
              height: "450px",
            }}
          >
            <Container component="form" sx={{ mt: 1 }}>
              <Typography variant="h2">✅</Typography>
              <Typography color="#134074" variant="h5" fontWeight="bold">
                Password reset
              </Typography>
              <Typography color="#134074" variant="subtitle1">
                your password has been reseted. <br /> Click the button below to
                continue.
              </Typography>
              <button className="registerSubmitButton">continue</button>
            </Container>
          </Box>
        </Grid>
        <Grid item xs={1}></Grid>
      </Grid>
    </>
  );
};

export default ResetSuccess;
