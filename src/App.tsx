import "./App.css";
import Dashboard from "./pages/dashboard";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import Login from "./pages/login";
import Library from "./pages/bookLibrary";
import Register from "./pages/register";
import ForgotPassword from "./pages/forgotpassword";
import EmailSent from "./pages/emailsent";
import PasswordReset from "./pages/passwordreset";
import ResetSuccess from "./pages/resetsuccess";
import React from "react";
import NotificationsPage from "./pages/notification/index";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/register">
          <Register />
        </Route>

        <Route path="/forgotpassword">
          <ForgotPassword />
        </Route>

        <Route path="/emailsent">
          <EmailSent />
        </Route>

        <Route path="/passwordreset">
          <PasswordReset />
        </Route>

        <Route path="/resetsuccess">
          <ResetSuccess />
        </Route>

        <Route path="/notification">
          <NotificationsPage/>
        </Route>

        <Route path="/dashboard">
          <Dashboard />
        </Route>

        <Route path="/library">
          <Library />
        </Route>

        <Route path="/">
          <Login />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
