import React from "react";
import { render,screen  } from "@testing-library/react";
import Login from "../pages/login";
import { Dashboard } from "@mui/icons-material";
import userEvent from '@testing-library/user-event';
import LibraryMiniCard from "../component/libraryMiniCard";
import App from "../App";


describe("LibraryMiniCard", () => {
  it("should render a text in login page", () => {
    const { getByText } = render(<Login />);
    expect(getByText("MARIBACA")).toBeInTheDocument();
  });
});


test('Test Login Anchor Text', ()=>{
  render(<App/>);
  const button2 = screen.getByText("Create Now!");
  userEvent.click(button2);
})