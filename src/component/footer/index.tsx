import React, { FC, useEffect, useState } from "react";
import { Box } from "@mui/material";

const Footer: FC = () => {
  return (
    <>
      <Box>
        <p>© 2022 Maribaca All Right Reserved</p>
      </Box>
    </>
  );
};

export default Footer;
