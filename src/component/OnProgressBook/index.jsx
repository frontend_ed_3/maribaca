import * as React from 'react';
import { useState } from 'react';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";
import { CardActionArea } from '@mui/material';
import Card from '@mui/material/Card';
import { useOnProgress } from '../progressCard/store';
import { useProgressLevels } from '../progressBar/store';
import { withProvider } from '../progressBar/store';
const OnProgressBookCard =(props)=>{
    const [open, setOpen] = React.useState(false);
    const [newBookPageProgress, setNewBookPageProgress] = useState(0);
    const { updateProgress } = useOnProgress();
    const { addProgressLevel} = useProgressLevels();
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const storeData = () => {
        updateProgress(newBookPageProgress, props.library.id);
        // readerLevel(newBookPageProgress);
        addProgressLevel(newBookPageProgress);
        setNewBookPageProgress();
        handleClose();
      };
    
    const style = {
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        width: 300,
        bgcolor: "background.paper",
        boxShadow: 24,
        p: 4,
        outline: "none",
        borderRadius:"20px",
      };
    const progresPercentage = Math.floor(((props.library.progress)/props.library.pages)*100);
    return (
    <>
       
        <Card sx={{ width: 280, marginTop: 2,marginRight:2, maxHeight:120, alignItems: 'center'}}>    
        <CardActionArea sx={{ padding: 1 }}  onClick={props.library.pages <= props.library.progress ? handleClose :handleOpen} >    
            <Grid container  >
                <Grid item xs={4} >
                    <CardMedia
                    component="img"
                    height="72"                            
                    image="https://t3.ftcdn.net/jpg/02/89/96/04/360_F_289960403_ysSwxqddDRMixSxj4U6F8KpgvnhMh9XW.jpg"
                    alt="Book Image"
                    sx={{ maxWidth: 72 }}
                    />
                </Grid>
                <Grid item xs={8}>
                <CardContent sx={{ height:30, padding:1 }}>                                                           
                    <Typography variant="body2" color="text.primary" sx={{ fontSize: 12 }} fontFamily={'nuito'}>
                        {props.library.title}                           
                    </Typography>   
                    <Typography variant="body2" color="text.secondary" sx={{ fontSize: 11 }} fontFamily={'nuito'}>
                    {props.library.pages <= props.library.progress? <p> Progress: Complete</p> : <p>Progress: {progresPercentage}%</p> }                            
                    </Typography>                                                                
                </CardContent>                
                </Grid>                            
            </Grid> 
        </CardActionArea>
    </Card> 
    <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description">
        <Box sx={style}>
        <Typography id="modal-modal-title" variant="h6" component="h2" fontFamily={'nuito'}>
            <b>Add Your Reading Progress</b> <br />
            <p style={{ fontSize: 14, margin: 0 }}>fill in the page you have been read</p>
            <hr />
        </Typography>
        <Typography id="modal-modal-description" sx={{ mt: 2 }} fontFamily={'nuito'}>
            <Box
            component="form"
            sx={{
                "& > :not(style)": { m: 0, width: "100%", mb: 1 },
            }}
            noValidate
            autoComplete="off"
            >
            <TextField
                id="filled-basic"
                label="Number of book page you have read"
                type="number"
                variant="filled"
                onChange={(e) => setNewBookPageProgress(parseInt(e.target.value))}
            />
            </Box>
        </Typography>
        <Stack
            spacing={2}
            direction="row"
            style={{ float: "right", marginTop: 20 }}
        >
            <Button variant="text" onClick={handleClose}>
            Cancel
            </Button>
            <Button variant="contained" onClick={storeData}>
            Save
            </Button>
        </Stack>
        </Box>
    </Modal>
    </>

    )
}
export default withProvider(OnProgressBookCard);