import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { useState,useEffect } from 'react';
import AddBoxIcon from '@mui/icons-material/AddBox';
import Grid from '@mui/material/Grid';
import BookCard from '../BookCard/index';
import Pagination from '@mui/material/Pagination';
import usePagination from '../Pagination/paginationHandler';

const LibraryMiniCard= ()=> {
    const [items, setItems] = useState([]);
    let [page, setPage] = useState(1);

useEffect(() => {
  const items = JSON.parse(localStorage.getItem('libraryBooks'));
  if (items) {
   setItems(items);
  }
}, []);

  const PER_PAGE = 3;

  const count = Math.ceil(items.length / PER_PAGE);
  const _DATA = usePagination(items, PER_PAGE);

  const handleChange = (e, p) => {
    setPage(p);
    _DATA.jump(p);
  };
  return (
    <Card container sx={{ width: 300 , maxHeight: 500, alignItems: 'center', padding:3,borderRadius:5}}>
        <Typography variant="h6" gutterBottom component="div" fontFamily={'nuito'}>
                My Library
        </Typography>
        <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center">
            
            <CardActions>
                <Button variant="contained" startIcon={<AddBoxIcon />} href="/library" >
                    Add New Book
                </Button>
            </CardActions> 
            <Grid sx={{maxHeight:300}}>            
            <CardContent sx={{ width: 280 ,maxHeight: 300, alignItems: 'center', padding:1, borderRadius:5, backgroundColor:"#8DA9C4"}}>
            {items != null &&
                _DATA.currentData().map((data) => (
                    <BookCard key={data.id} library={data}/>
            ))}            
            </CardContent>
            </Grid> 
            {items &&
                <Pagination 
                  sx={{ marginTop:4}}
                  count={count}
                  size="large"
                  page={page}
                  variant="outlined"
                  shape="rounded"
                  onChange={handleChange}/>}
            
        </Grid>                
    </Card>
  );
}

export default LibraryMiniCard;