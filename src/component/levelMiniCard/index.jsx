import React from "react";
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import LinearWithValueLabel from '../progressBar';

export default function LevelMiniCard() {
  // const [level, setLevel] = useState([]);
  // console.log(level)
  const items = JSON.parse(localStorage.getItem("progressLevels"));

  return (
    <Card sx={{ minWidth: 300 ,maxHeight: 414, alignItems: 'center', padding:1, marginBottom:2}}>
        {items ? <Typography variant="h6" gutterBottom component="div" fontFamily={'nuito'}>
                Level {items.level}
        </Typography>:
        <Typography variant="h6" gutterBottom component="div" fontFamily={'nuito'}>
        Level 0
        </Typography>}
        <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center">
            <LinearWithValueLabel/>
                                   
        </Grid>
        

    </Card>
  );
}