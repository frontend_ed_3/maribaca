import * as React from 'react';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import { CardActionArea } from '@mui/material';
import Card from '@mui/material/Card';

const BookCard =(props)=>{
    console.log(props.library.title)
    return (
        <Card sx={{ maxWidth: 340, marginTop: 1 }}>    
        <CardActionArea sx={{ padding: 1 }}>    
            <Grid container spacing={2} >
                <Grid item xs={4} >
                    <CardMedia
                    component="img"
                    height="72"                            
                    image="https://t3.ftcdn.net/jpg/02/89/96/04/360_F_289960403_ysSwxqddDRMixSxj4U6F8KpgvnhMh9XW.jpg"
                    alt="Book Image"
                    sx={{ maxWidth: 72 }}
                    />
                </Grid>
                <Grid item xs={8}>
                <CardContent>                                                           
                    <Typography variant="body2" fontFamily={'nuito'} color="text.secondary" sx={{ fontSize: 12 }}>
                        {props.library.title}                              
                    </Typography>                                
                </CardContent>
                </Grid>                            
            </Grid> 
        </CardActionArea>
    </Card>        

    )
}
export default BookCard;