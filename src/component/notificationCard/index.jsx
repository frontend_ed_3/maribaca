import * as React from 'react';
import Card from '@mui/material/Card';

import CardContent from '@mui/material/CardContent';

import Typography from '@mui/material/Typography';
import { useState,useEffect } from 'react';

import Grid from '@mui/material/Grid';
import Pagination from '@mui/material/Pagination';
import usePagination from '../Pagination/paginationHandler';
import NotificationMiniCard from './notificationMiniCard';

const NotificationCard= ()=> {
    const [items, setItems] = useState([]);
    let [page, setPage] = useState(1);

useEffect(() => {
  const items = JSON.parse(localStorage.getItem("progressBooks"));
  if (items) {
   setItems(items);
  }
  console.log(_DATA)
}, []);

  const PER_PAGE = 10;

  const count = Math.ceil(items.length / PER_PAGE);
  const _DATA = usePagination(items, PER_PAGE);

  const handleChange = (e, p) => {
    setPage(p);
    _DATA.jump(p);
  };
  return (
    <Card container sx={{ width: 300 , maxHeight: 500, alignItems: 'center', padding:3,borderRadius:5}}>
        <Typography variant="h6" gutterBottom component="div" fontFamily={'nuito'}>
                Notification
        </Typography>
        <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center">
            
            {/* <CardActions>
                <Button variant="contained" startIcon={<AddBoxIcon />} href="/library" >
                    Add New Book
                </Button>
            </CardActions>  */}
            <Grid sx={{maxHeight:300}}>            
            <CardContent sx={{ width: 280 ,maxHeight: 300, alignItems: 'center', padding:1, borderRadius:5, backgroundColor:"#8DA9C4"}}>
            {items != null &&
                _DATA.currentData().map((data) => (
                    <NotificationMiniCard key={data.id} library={data}/>
            ))}            
            </CardContent>
            </Grid> 
            {items &&
                <Pagination 
                  sx={{ marginTop:4}}
                  count={count}
                  size="large"
                  page={page}
                  variant="outlined"
                  shape="rounded"
                  onChange={handleChange}/>}
            
        </Grid>                
    </Card>
  );
}

export default NotificationCard;