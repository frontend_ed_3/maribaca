import * as React from 'react';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';

export default function PaginationCard() {
  return (
    <Stack spacing={2}>
      <Pagination count={10} shape="rounded" size="small"/>      
    </Stack>
  );
}