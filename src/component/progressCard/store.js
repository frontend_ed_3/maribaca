import React, { useContext, createContext } from "react";
import { useState,useEffect } from 'react';

const Context = createContext({
  onProgressBooks: [],
});

const Provider = (props) => {
  const items = JSON.parse(localStorage.getItem('progressBooks'));
  const { children } = props;
  const [onProgressBooks, setOnProgressBooks] = useState([]);
  const [data, setData] = useState(items);

  useEffect(() => {
    if (items) {
     setOnProgressBooks(items);
    }
  }, []); 

  const updateProgress = (completed,itemId) => {
    const newState = data.map(obj => {
     
      if (obj.id === itemId) {
        return {...obj,progress: completed};
      }
     
      return obj;
    });    
    setData(newState);
    // console.log(newState)
    localStorage.setItem('progressBooks', JSON.stringify(newState));
  };
  return (
    <Context.Provider value={{ onProgressBooks, updateProgress }}>
      {children}
    </Context.Provider>
  );
};

export const useOnProgress = () => useContext(Context);

export const withProvider = (Component) => {
  return (props) => {
    return (
      <Provider>
        <Component {...props} />
      </Provider>
    );
  };
};
