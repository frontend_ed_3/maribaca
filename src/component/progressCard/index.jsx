import * as React from "react";
import Card from "@mui/material/Card";
import Typography from "@mui/material/Typography";
import { useState, useEffect } from "react";
import Grid from "@mui/material/Grid";
import Pagination from "@mui/material/Pagination";
import usePagination from "../Pagination/paginationHandler";
import OnProgressBookCard from "../OnProgressBook";
import { withProvider } from "./store";

const ProgressCard = () => {
  const [items, setItems] = useState([]);
  let [page, setPage] = useState(1);

  useEffect(() => {
    const items = JSON.parse(localStorage.getItem("progressBooks"));
    if (items) {
      setItems(items);
    }
  }, [items]);

  const PER_PAGE = 4;

  const count = Math.ceil(items.length / PER_PAGE);
  const _DATA = usePagination(items, PER_PAGE);

  const handleChange = (e, p) => {
    setPage(p);
    _DATA.jump(p);
  };

  // const data = JSON.parse(localStorage.getItem("libraryBooks"));
  // console.log(items);
  return (
    <Card container sx={{ width: 700 ,maxHeight: 500, alignItems: 'center', padding:3,borderRadius:5}}>
        <Typography variant="h6" gutterBottom component="div" fontFamily={'nuito'}>
                On Progress ...
        </Typography>
        <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
      >
        <Grid
          container
          justifyContent="center"
          alignItems="center"
          sx={{ maxHeight: 350 }}
        >
          <Grid
            container
            justifyContent="center"
            alignItems="center"
            // spacing={{ xs: 2 }}
            columns={{ xs: 4, sm: 8, md: 12 }}
            sx={{ padding: 1, width: 680, maxHeight: 200 }}
          >
            {items != null &&
              _DATA
                .currentData()
                .map((data) => (
                  <OnProgressBookCard key={data.id} library={data} />
                ))}
          </Grid>
        </Grid>

        {items && (
          <Pagination
            sx={{ marginTop: 4 }}
            count={count}
            size="large"
            page={page}
            variant="outlined"
            shape="rounded"
            onChange={handleChange}
          />
        )}
      </Grid>
    </Card>
  );
};

export default withProvider(ProgressCard);
