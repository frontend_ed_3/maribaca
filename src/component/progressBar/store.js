import React, { useContext, createContext, useState, useEffect } from "react";
import useLocalStorage from "../../hooks/useLocalStorage";

const Context = createContext({
  progressLevel: [],
});

const Provider = (props) => {

  const { children } = props;
  const [level,setLevel] = useState(1);

  const [progressLevel, setProgressLevel] = useLocalStorage("progressLevels", {
    level: 1,
    current: 0,
    target:1000,

  });


useEffect(() => {
    // const items = JSON.parse(localStorage.getItem("progressLevels"));
    if (progressLevel.current >= progressLevel.target) {
      setLevel(progressLevel.level+1);
    }
    else setLevel(progressLevel.level);
  }, [progressLevel]);

  
  
  const addProgressLevel = ( 
    current) => {
    const items = JSON.parse(localStorage.getItem('progressBooks'));
    const readerTarget =
    Math.round( 0.5 * (level ^ 5) + 0.8 * (level ^ 6) + 300 * level);
    const allProgress = items.map( obj => obj.progress * 1);
    const total = allProgress.reduce( (a,b) => (a+b) );
    const progress = {
        level: level,
        current: total,
        target: readerTarget,
    };
    setProgressLevel(progress);
    
  };
  return (
    <Context.Provider value={{ progressLevel, addProgressLevel}}>
      {children}
    </Context.Provider>
  );
};

export const useProgressLevels = () => useContext(Context);

export const withProvider = (Component) => {
  return (props) => {
    return (
      <Provider>
        <Component {...props} />
      </Provider>
    );
  };
};