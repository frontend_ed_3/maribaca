import React, { useContext, createContext } from "react";
import useLocalStorage from "../../../hooks/useLocalStorage";

const Context = createContext({
  progressBooks: [],
});

const Provider = (props) => {
  const { children } = props;
  const [progressBooks, setProgressBooks] = useLocalStorage("progressBooks", []);

  const addReadingMaterial = ( 
    title,
    pages,
    progress,
    timeStamp,) => {
    const nextId =
      progressBooks.length > 0 ? Math.max(...progressBooks.map((b) => b.id)) + 1 : 0;
    const newBook = {
      id: nextId,
      title,
      pages,
      progress,
      timeStamp,
    };
    setProgressBooks([...progressBooks, newBook]);
  };
  return (
    <Context.Provider value={{ progressBooks, addReadingMaterial}}>
      {children}
    </Context.Provider>
  );
};

export const useProgressBooks = () => useContext(Context);

export const withProvider = (Component) => {
  return (props) => {
    return (
      <Provider>
        <Component {...props} />
      </Provider>
    );
  };
};