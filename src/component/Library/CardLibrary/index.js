import React, { useEffect, useState } from "react";
import { withProvider } from "./store";
import FormRow from "./formRow";

const Todos = () => {
  const [items, setItems] = useState([]);

  useEffect(() => {
    const items = JSON.parse(localStorage.getItem("libraryBooks"));
    if (items) {
      setItems(items);
    }
  }, [items]);

  // const data = JSON.parse(localStorage.getItem("libraryBooks"));
  // console.log(items);
  return (
    <>
      {items != null && items.map((library) => <FormRow library={library} key={library.id} />)}
    </>
  );
};

export default withProvider(Todos);
