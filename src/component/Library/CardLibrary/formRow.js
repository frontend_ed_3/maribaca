import "./index.css";
import React from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import {  Grid } from "@mui/material";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import {  useProgressBooks } from "./store";
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import { useLibraryBooks } from "../AddButton/store";
import { withProvider } from "../AddButton/store";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 300,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
  outline: "none",
  fontFamily:'nuito',
  borderRadius:'20px',
};

const  FormRow =(props)=> {
  // console.log(props);
  const [open, setOpen] = React.useState(false);
  const [value, setValue] = React.useState(new Date('2022-01-01T00:00:00'));
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const handleChange = (newValue) => {
    setValue(newValue);
  };
  const { addReadingMaterial } = useProgressBooks();
  const { removeLibraryBook } = useLibraryBooks();
  const onClick = () => {
    addReadingMaterial(
      props.library.title,
      props.library.pages,
      props.library.progress,
      value);
    removeLibraryBook(props.library.id);
    handleClose();
  };

  return (
    <React.Fragment>
      <Grid item marginLeft={3}>
        <Card className="bookcard" sx={{ maxWidth: 350, borderRadius: "16px" }}>
          <CardMedia
            backgroundColor="#8DA9C4" 
            component="img"
            height="350"
            image="https://t3.ftcdn.net/jpg/02/89/96/04/360_F_289960403_ysSwxqddDRMixSxj4U6F8KpgvnhMh9XW.jpg"
            alt="example book"
          />
          <CardContent>
            <Typography
              className="title"
              gutterBottom
              variant="h5"
              component="div"
              fontFamily={'nuito'}
            >
              {props.library.title}
            </Typography>
            <Typography variant="body2" color="text.secondary" fontFamily={'nuito'}>
              Page : {props.library.pages} Pages
            </Typography>
          </CardContent>
          <CardActions onClick={handleOpen}>
            <AddCircleOutlineIcon
              sx={{ fontSize: 50 }}              
            />Start Reading            
          </CardActions>
        </Card>
      </Grid>

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2" fontFamily={'nuito'}>
            <b>Inform Your Reading Target</b> <br />
            <p style={{ fontSize: 14, margin: 0 }}>fill in the due date</p>
            <hr />
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }} fontFamily={'nuito'}>
            <Box
              component="form"
              sx={{
                "& > :not(style)": { m: 0, width: "100%", mb: 1 },
              }}
              noValidate
              autoComplete="off"
            >
               <LocalizationProvider dateAdapter={AdapterDateFns}>
                  <Stack spacing={3}>
                    <DesktopDatePicker
                      label="Reading Due Date"
                      inputFormat="MM/dd/yyyy"
                      value={value}
                      onChange={handleChange}
                      renderInput={(params) => <TextField {...params} />}
                    />
                  </Stack>
                </LocalizationProvider>
            </Box>
          </Typography>
          <Stack
            spacing={2}
            direction="row"
            style={{ float: "right", marginTop: 20 }}
          >
            <Button variant="text" onClick={handleClose}>
              Cancel
            </Button>
            <Button
              variant="contained"
              onClick={onClick}
            >
              Save
            </Button>
          </Stack>
        </Box>
      </Modal>
    </React.Fragment>
  );
}

export default withProvider(FormRow);
