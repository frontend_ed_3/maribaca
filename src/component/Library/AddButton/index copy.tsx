import * as React from "react";
import "./index.css";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import CardActions from "@mui/material/CardActions";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import Todos from "../CardLibrary/index"

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
  outline: "none",
};

export default function addbutton(): JSX.Element {
  const [open, setOpen] = React.useState(false);
  const [bookName, setBookName] = React.useState("");
  const [bookPages, setBookPages] = React.useState("");

  const storeData = () => {
    localStorage.setItem("bookname", bookName);
    localStorage.setItem("bookpages", bookPages);
    Todos();
    
  };

  const removeData = () => {
    localStorage.removeItem("bookname");
    localStorage.removeItem("bookpages");
  };

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  return (
    <React.Fragment>
      <Stack direction="row" spacing={2} className="addbutton">
        <Button
          className="addbutton"
          variant="contained"
          sx={{ maxWidth: 400, borderRadius: "16px" }}
        >
          <CardActions>
            <AddCircleOutlineIcon
              sx={{ fontSize: 150 }}
              onClick={handleOpen}
            ></AddCircleOutlineIcon>
          </CardActions>
        </Button>
      </Stack>

      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2" fontFamily={'nuito'}>
            <b>Inform your Book</b> <br />
            <p style={{ fontSize: 14, margin: 0 }}>fill in the book details</p>
            <hr />
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }} fontFamily={'nuito'}>
            <Box
              component="form"
              sx={{
                "& > :not(style)": { m: 0, width: "100%", mb: 1 },
              }}
              noValidate
              autoComplete="off"
            >
              <TextField
                id="filled-basic"
                label="Book Name"
                variant="filled"
                value={bookName}
                onChange={(e) => setBookName(e.target.value)}
              />
              <TextField
                id="filled-basic"
                label="Number of book pages"
                variant="filled"
                value={bookPages}
                onChange={(e) => setBookPages(e.target.value)}
              />
            </Box>
          </Typography>
          <Stack
            spacing={2}
            direction="row"
            style={{ float: "right", marginTop: 20 }}
          >
            <Button variant="text" onClick={handleClose}>
              Cancel
            </Button>
            <Button variant="contained" onClick={storeData}>
              Save
            </Button>
          </Stack>
        </Box>
      </Modal>
    </React.Fragment>
  );
}
