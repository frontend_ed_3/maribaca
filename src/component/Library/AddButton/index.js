import React from "react";
import { withProvider } from "./store";
import AddLibraryBook from "./addLibraryBook";

const AddButton = () => {
  return (
    <>
      <AddLibraryBook />
    </>
  );
};

export default withProvider(AddButton);
