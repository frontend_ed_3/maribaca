import React, { useContext, createContext } from "react";
import useLocalStorage from "../../../hooks/useLocalStorage";

const Context = createContext({
  libraryBooks: [],
});

const Provider = (props) => {
  const { children } = props;
  const [libraryBooks, setlibraryBooks] = useLocalStorage("libraryBooks", []);
  const addLibraryBook = (title, pages) => {
    const nextId =
      libraryBooks.length > 0
        ? Math.max(...libraryBooks.map((b) => b.id)) + 1
        : 0;
    const newLibraryBook = {
      id: nextId,
      title,
      pages,
      completed: false,
      progress:0,
    };
    setlibraryBooks([...libraryBooks, newLibraryBook]);
  };
  const removeLibraryBook = (id) => {
    const newLibrary = libraryBooks.filter((b) => b.id !== id);
    setlibraryBooks(newLibrary);
  };
  return (
    <Context.Provider value={{ libraryBooks, addLibraryBook,removeLibraryBook }}>
      {children}
    </Context.Provider>
  );
};

export const useLibraryBooks = () => useContext(Context);

export const withProvider = (Component) => {
  return (props) => {
    return (
      <Provider>
        <Component {...props} />
      </Provider>
    );
  };
};
