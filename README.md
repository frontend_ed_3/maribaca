[![](https://img.shields.io/badge/TYPESCRIPT%20-%233178C6.svg?&style=flat&logo=typescript&logoColor=white)](https://typescriptlang.org) [![](https://img.shields.io/badge/REACT%20-%2356BDDA.svg?&style=flat&logo=react&logoColor=white)](https://reactjs.org)
# Documentation
## FE_ED_3 Generasi Gigih
### Ferdinand Chandra as mentor
* Ariq Nurmansyah - as Team Leader
* Aulia Imanni Jannewton - as Team Member
* Asyrie Chaeranie - as Team Member
* Sharhan Anhar - as Team Member
* Achmad Viqih Nurfauzi - as Team Member

## MARIBACA  
The MARIBACA application focuses on increasing literacy interest by providing several features such as add reading progress where in this feature the user can see the presentation of the progress of the book being read. The purpose of MARIBACA is to increase interest in reading and foster interest in reading in Indonesia because based on a survey of 80 countries, Indonesia is in the 75th position in reading interest. Data from UNESCO shows that the percentage of children who like to read is only 0.01%. That is, only 1 in 1000 children in Indonesia who like to read. (Kompasiana). The hope in Maribaca is that the Indonesian people, especially the generation z, have started to change their habits in wasting their daily time filled with reading, because by reading we can add insight and knowledge.

## Project goals  
The MARIBACA application project is expected to provide several benefits, especially for Generation Z, such as :
1. improve user habits from wasting time to reading books
2. Cultivate and increase users' reading interest
3. increased user insight and knowledge
4. Make the user disciplined towards the completion of the book that is read according to the target that the user has set by self
5. Making the percentage of reading progress a motivation to finish the book you are reading
6. As a daily reminder for users to read books

## FEATURES
1. Account Registration Feature
2. User Login and Log Out Features
3. Add Book and Add New Book features to add books that the user will read
4. Library feature as a place to store books that users will read
5. Inform your target feature to add a target for each book to be read
6. Add Reading Progress feature to enter how many pages have been read
7. Reading Progress feature to see the percentage of book pages that have been read
8. feature level that can be increased based on the type of book/reading that has been read by the user.
9. The notification feature is used as a reminder for the user to read and finish the book the user is reading

## Tech Stack Use
1. JavaScript and typescript as programming languages
2. ReactJS as a javascript library
3. Material UI as Website Framework
4. Local Storage as data storage on the web browser
5. Redux
6. Flex & Grid
7. Hooks
8. Testing
9. Eslint

## INSTALLATION AND RUN PROGRAM
### HOW TO RUN PROGRAM WITHOUT INSTALLATION?
You can just click this link (https://maribaca-fe-ed3.vercel.app/) and login, after that you can try to run the program without having to install anything.

### HOW TO RUN PROGRAM WITH INSTALLATION
1. Download this github sourcecode, you can download with zip
2. Open the sourcode with visual studio code or with another editor
3. Instal and download the required package, use `npm install`
4. After done installed the package, klik terminal and type `npm start`
5. Your program will run in your laptop